// import axios from '@nuxtjs/axios'
export const state=()=>({
    mountains:[{
        id:12,
        continent:'hello'
    }],
    listTitle:['Avenger']
})
export const mutations={
    setMountains(state,payload){
        state.mountains=payload
    },
    addNewTitle(state,payload){
        state.listTitle.push(payload)
    }
    
}
export const actions={
     async setMountains(state){
         const mountains= await fetch(' https://api.nuxtjs.dev/mountains').then(res=>res.json())
         state.commit('setMountains',mountains)
    
     },
     addTitle({commit},payload){
        commit('addNewTitle',payload)
     },
     async post(){
    
         let res= await this.$axios.post('https://jsonplaceholder.typicode.com/posts',{title:"hello"})
         console.log(res)
     }
    
}
export const getters={
    getMountains:(state)=>{
        return state.mountains
    },
    getMountainByid:(state)=>(id)=>{
        return id
    },
    getAllTitle(state){
        return state.listTitle
    }

}
// https://api.nuxtjs.dev/mountains